bFit is an activity tracker watchface for Pebble smartwatch. It can track your daily steps, distance traveled, active minutes and even track your sleep. bFit motivates you to be healthy and active.
