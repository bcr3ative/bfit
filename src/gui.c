#include <pebble.h>
#include "gui.h"

// Window definition
static Window *window;

// Layer defintions
static Layer *status_layer;
static Layer *info_layer;
static Layer *sep;

// Text layer definitions
static TextLayer *date_layer;
static TextLayer *battery_layer;
static TextLayer *time_layer;
static TextLayer *tracking_layer;
static TextLayer *steps_layer;
static TextLayer *distance_layer;
static TextLayer *active_layer;
static TextLayer *sleep_layer;

// Private function prototypes
static void draw_status_layer(Layer *layer, GContext *ctx);
static void draw_info_layer(Layer *layer, GContext *ctx);
static void draw_sep(Layer *layer, GContext *ctx);

void init_gui() {
	// Create window to display the UI on
	window = window_create();
	window_stack_push(window, true);

	Layer *window_layer = window_get_root_layer(window);

	// Status layer that contains the date and battery charge
	status_layer = layer_create(GRect(0, 0, 144, 24));
	layer_set_update_proc(status_layer, draw_status_layer);
	layer_add_child(window_layer, status_layer);

	// Date layer contains the date
	date_layer = text_layer_create((GRect) { .origin = { 10, 0 }, .size = { 62, 18 } });
	text_layer_set_background_color(date_layer, GColorClear);
	text_layer_set_text_color(date_layer, GColorWhite);
	text_layer_set_font(date_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD));
	text_layer_set_text_alignment(date_layer, GTextAlignmentCenter);
	layer_add_child(status_layer, text_layer_get_layer(date_layer));

	// Battery layer contains the battery status information
	battery_layer = text_layer_create((GRect) { .origin = { 72, 0 }, .size = { 62, 18 } });
	text_layer_set_background_color(battery_layer, GColorClear);
	text_layer_set_text_color(battery_layer, GColorWhite);
	text_layer_set_font(battery_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD));
	text_layer_set_text_alignment(battery_layer, GTextAlignmentCenter);
	layer_add_child(status_layer, text_layer_get_layer(battery_layer));

	// Time layer contains the time
	time_layer = text_layer_create((GRect) { .origin = { 0, 24 }, .size = { 144, 56 } });
	text_layer_set_background_color(time_layer, GColorClear);
	text_layer_set_text_color(time_layer, GColorBlack);
	text_layer_set_font(time_layer, fonts_get_system_font(FONT_KEY_ROBOTO_BOLD_SUBSET_49));
	text_layer_set_text_alignment(time_layer, GTextAlignmentCenter);
	layer_add_child(window_layer, text_layer_get_layer(time_layer));

	tracking_layer = text_layer_create((GRect) { .origin = { 61, 70 }, .size = { 22, 14 } });
	text_layer_set_background_color(tracking_layer, GColorClear);
	text_layer_set_text_color(tracking_layer, GColorBlack);
	text_layer_set_font(tracking_layer, fonts_get_system_font(FONT_KEY_GOTHIC_14));
	text_layer_set_text_alignment(tracking_layer, GTextAlignmentCenter);
	layer_add_child(window_layer, text_layer_get_layer(tracking_layer));

	// Info layer contains a black rectangle UI element
	info_layer = layer_create(GRect(0, 86, 144, 82));
	layer_set_update_proc(info_layer, draw_info_layer);
	layer_add_child(window_layer, info_layer);

	// Steps layer contains the steps information
	steps_layer = text_layer_create((GRect) { .origin = { 10, 0 }, .size = { 62, 36 } });
	text_layer_set_background_color(steps_layer, GColorClear);
	text_layer_set_text_color(steps_layer, GColorWhite);
	text_layer_set_font(steps_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD));
	text_layer_set_text_alignment(steps_layer, GTextAlignmentCenter);
	layer_add_child(info_layer, text_layer_get_layer(steps_layer));

	// Distance layer contains the travelled distance information
	distance_layer = text_layer_create((GRect) { .origin = { 72, 0 }, .size = { 62, 36 } });
	text_layer_set_background_color(distance_layer, GColorClear);
	text_layer_set_text_color(distance_layer, GColorWhite);
	text_layer_set_font(distance_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD));
	text_layer_set_text_alignment(distance_layer, GTextAlignmentCenter);
	layer_add_child(info_layer, text_layer_get_layer(distance_layer));

	// Separator
	sep = layer_create(GRect(10, 41, 124, 1));
	layer_set_update_proc(sep, draw_sep);
	layer_add_child(info_layer, sep);

	// Active layer contains the total time the users has been active
	active_layer = text_layer_create((GRect) { .origin = { 10, 40 }, .size = { 62, 36 } });
	text_layer_set_background_color(active_layer, GColorClear);
	text_layer_set_text_color(active_layer, GColorWhite);
	text_layer_set_font(active_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD));
	text_layer_set_text_alignment(active_layer, GTextAlignmentCenter);
	layer_add_child(info_layer, text_layer_get_layer(active_layer));

	// Sleep layer contains the total time the user has been sleeping
	sleep_layer = text_layer_create((GRect) { .origin = { 72, 40 }, .size = { 62, 36 } });
	text_layer_set_background_color(sleep_layer, GColorClear);
	text_layer_set_text_color(sleep_layer, GColorWhite);
	text_layer_set_font(sleep_layer, fonts_get_system_font(FONT_KEY_GOTHIC_18_BOLD));
	text_layer_set_text_alignment(sleep_layer, GTextAlignmentCenter);
	layer_add_child(info_layer, text_layer_get_layer(sleep_layer));
}

static void draw_status_layer(Layer *layer, GContext *ctx) {
	graphics_context_set_fill_color(ctx, GColorBlack);
	graphics_fill_rect(ctx, layer_get_bounds(layer), 5, GCornersBottom);
}

static void draw_info_layer(Layer *layer, GContext *ctx) {
	graphics_context_set_fill_color(ctx, GColorBlack);
	graphics_fill_rect(ctx, layer_get_bounds(layer), 5, GCornersTop);
}

static void draw_sep(Layer *layer, GContext *ctx) {
	graphics_context_set_fill_color(ctx, GColorWhite);
	graphics_fill_rect(ctx, layer_get_bounds(layer), 0, GCornerNone);
}

void deinit_gui() {
	// Destroy layers in reverse order of creation
	text_layer_destroy(date_layer);
	text_layer_destroy(battery_layer);
	text_layer_destroy(steps_layer);
	text_layer_destroy(distance_layer);
	text_layer_destroy(active_layer);
	text_layer_destroy(sleep_layer);
	layer_destroy(sep);
	layer_destroy(info_layer);
	layer_destroy(status_layer);
	text_layer_destroy(time_layer);
	text_layer_destroy(tracking_layer);

  	// And lastly destroy the window
	window_destroy(window);
}

void update_layer_content(enum LayerType layer, struct LayerData data) {
	// ex. Jan 01
	static char date_buf[7];
	// ex. 100%
	static char battery_buf[5];
	// ex. 00:00
	static char time_buf[6];
	// ex. ACT
	static char tracking_buf[4];
	// ex. Steps\n00000
	static char steps_buf[12];
	// ex. Distance\n00.0km
	static char distance_buf[16];
	// ex. Active\n00:00
	static char active_buf[13];
	// ex. Sleep\n00:00
	static char sleep_buf[12];

	switch (layer) {
		case 1:
			// Date
			strftime(date_buf, sizeof(date_buf), data.str_format, data.content.tick_time);
			text_layer_set_text(date_layer, date_buf);
			break;
		case 2:
			// Battery status
			snprintf(battery_buf, sizeof(battery_buf), "%d%%", (uint8_t)data.content.number);
			text_layer_set_text(battery_layer, battery_buf);
			break;
		case 3:
			// Time
			if (!data.str_format) {
				strncpy(time_buf, data.content.string, sizeof(time_buf));
			} else {
				strftime(time_buf, sizeof(time_buf), data.str_format, data.content.tick_time);
			}
			text_layer_set_text(time_layer, time_buf);
			break;
		case 4:
			// Tracking status
			snprintf(tracking_buf, sizeof(tracking_buf), "%s", data.content.string);
			text_layer_set_text(tracking_layer, tracking_buf);
			break;
		case 5:
			// Steps status
			snprintf(steps_buf, sizeof(steps_buf), "Steps\n%lu", data.content.number);
			text_layer_set_text(steps_layer, steps_buf);
			break;
		case 6:
			// Distance status
			if (!distance_unit) {
				snprintf(distance_buf, sizeof(distance_buf), "Distance\n%lu.%lukm", data.content.number / 1000, (data.content.number % 1000) / 100);
			} else {
				snprintf(distance_buf, sizeof(distance_buf), "Distance\n%lu.%lumi", data.content.number / 1000, (data.content.number % 1000) / 100);
			}
			text_layer_set_text(distance_layer, distance_buf);
			break;
		case 7:
			if (sleep_view) {
				// Deep sleep status
				snprintf(active_buf, sizeof(active_buf), "Deep\n%02d:%02d", (uint16_t)data.content.number / 60, (uint16_t)data.content.number % 60);
			} else {
				// Active status
				snprintf(active_buf, sizeof(active_buf), "Active\n%02d:%02d", (uint16_t)data.content.number / 60, (uint16_t)data.content.number % 60);
			}
			text_layer_set_text(active_layer, active_buf);
			break;
		case 8:
			if (sleep_view) {
				// Light sleep status
				snprintf(sleep_buf, sizeof(sleep_buf), "Light\n%02d:%02d", (uint16_t)data.content.number / 60, (uint16_t)data.content.number % 60);
			} else {
				// Sleep status
				snprintf(sleep_buf, sizeof(sleep_buf), "Sleep\n%02d:%02d", (uint16_t)data.content.number / 60, (uint16_t)data.content.number % 60);
			}
			text_layer_set_text(sleep_layer, sleep_buf);
			break;
		default:
			break;
	}
}