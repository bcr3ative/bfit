#ifndef GUI_H_INCLUDED
#define GUI_H_INCLUDED

extern bool distance_unit;
extern bool sleep_view;

// Layer display data
enum LayerType {
	DATE_L = 1,
	BATTERY_L = 2,
	TIME_L = 3,
	TRACKING_L = 4,
	STEPS_L = 5,
	DISTANCE_L = 6,
	ACTIVE_L = 7,
	SLEEP_L = 8
};

struct LayerData {
	union LayerContent {
		uint32_t number;
		const char *string;
		struct tm *tick_time;
	} content;
	const char *str_format;
};

// Function prototypes
void init_gui();
void deinit_gui();
void update_layer_content(enum LayerType layer, struct LayerData data);

#endif