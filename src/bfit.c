#include <pebble.h>
#include "gui.h"

#define ACTIVITY_TRESHOLD 200
#define SAMPLE_COUNT 25

#define TOTAL_STEPS_KEY 100
#define ACTIVE_MINUTES_KEY 101
#define DEEP_SLEEP_MINUTES_KEY 102
#define LIGHT_SLEEP_MINUTES_KEY 103
#define SLEEP_MODE_KEY 104
#define LAST_DATE_KEY 105
#define DISTANCE_UNIT_KEY 106

enum Settings { setting_distance = 1 };
static AppSync app;
static uint8_t buffer[256];

static uint32_t total_steps;
static uint16_t steps_per_minute;
static uint16_t active_minutes;
static uint16_t deep_sleep_minutes;
static uint16_t light_sleep_minutes;
static bool step_engaged;
static bool sleep_mode;
static bool is_tracking;
static char current_date[7];
static char last_date[7];

bool distance_unit;
bool sleep_view;

static void update_tracking_status(bool sleep);
static void sleep_view_handler(AccelAxisType axis, int32_t direction);
static void update_active_sleep();
static void reset_active_sleep();

typedef struct SamplesData {
	uint8_t active_axis;
	int16_t avg_value;
} SamplesData;

SamplesData get_active_samples_data(AccelData *data, uint32_t num_samples) {
	uint8_t i;
	int16_t avgs[3], max_i;
	int32_t sum_x = 0, sum_y = 0, sum_z = 0;
	SamplesData samples_data;

	for (i = 0; i < num_samples; i++) {
		sum_x += (data + i)->x;
		sum_y += (data + i)->y;
		sum_z += (data + i)->z;
	}

	avgs[0] = sum_x / SAMPLE_COUNT;
	avgs[1] = sum_y / SAMPLE_COUNT;
	avgs[2] = sum_z / SAMPLE_COUNT;

	for (i = 0; i < 3; i++) {
		if (i == 0) {
			max_i = 0;
		} else {
			if (abs(avgs[i]) >= abs(avgs[max_i])) {
				max_i = i;
			}
		}
	}
	samples_data.active_axis = max_i + 1;
	samples_data.avg_value = avgs[max_i];

	return samples_data;
}

// Function handles the accelerometer data
static void accel_data_handler(AccelData *data, uint32_t num_samples) {
	uint8_t i;
	uint16_t current_steps = 0;
	static uint8_t sleep_pattern_state = 0;
	static uint8_t sleep_pattern_timeout = 0;

	SamplesData samples_data = get_active_samples_data(data, num_samples);
	// APP_LOG(APP_LOG_LEVEL_DEBUG, "Axis: %d, AVG: %d", samples_data.active_axis, samples_data.avg_value);
	for (i = 0; i < num_samples; i++) {
		if ((data + i)->did_vibrate == false) {
			// X-Axis
			if (samples_data.active_axis == 1) {
				if (samples_data.avg_value > 0) {
					if ((data + i)->x >= samples_data.avg_value + ACTIVITY_TRESHOLD && step_engaged == false) {
						step_engaged = true;
					} else if ((data + i)->x <= samples_data.avg_value - ACTIVITY_TRESHOLD && step_engaged == true) {
						current_steps++;
						step_engaged = false;
					}
				}
			// Y-Axis
			} else if (samples_data.active_axis == 2) {
				if (samples_data.avg_value > 0) {
					if ((data + i)->y >= samples_data.avg_value + ACTIVITY_TRESHOLD && step_engaged == false) {
						step_engaged = true;
					} else if ((data + i)->y <= samples_data.avg_value - ACTIVITY_TRESHOLD && step_engaged == true) {
						current_steps++;
						step_engaged = false;
					}
				}
			// Z-Axis
			} else if (samples_data.active_axis == 3) {
				if (samples_data.avg_value < 0) {
					// APP_LOG(APP_LOG_LEVEL_DEBUG, "|%d|%d|%d", (data + i)->x, (data + i)->y, (data + i)->z);
					if ((data + i)->y > 1000 && (sleep_pattern_state == 0 || sleep_pattern_state == 2))
					{
						sleep_pattern_state++;
					} else if ((data + i)->y < -1700 && (sleep_pattern_state == 1 || sleep_pattern_state == 3)) {
						if (sleep_pattern_state == 3)
						{
							sleep_mode = !sleep_mode;
							vibes_double_pulse();
							sleep_pattern_state = 0;
							sleep_pattern_timeout = 0;
							update_tracking_status(sleep_mode);
							if (sleep_mode) {
								deep_sleep_minutes = 0;
								light_sleep_minutes = 0;
								update_active_sleep();
							}
							// Wait for the next minute to start tracking
							is_tracking = false;
						} else {
							sleep_pattern_state++;
						}
					}
				}
			}
		}
	}

	if (sleep_pattern_state > 0 && sleep_pattern_timeout < 2) {
		sleep_pattern_timeout++;
	}
	if (sleep_pattern_timeout == 2) {
		sleep_pattern_state = 0;
		sleep_pattern_timeout = 0;
	}

	steps_per_minute += current_steps;

	if (sleep_mode == false) {
		total_steps += current_steps;
		update_layer_content(STEPS_L, (struct LayerData) { { .number = total_steps }, NULL });
	}
}

static uint32_t get_total_distance() {
	uint32_t total_distance;

	total_distance = (total_steps * 7) / 10;
	if (distance_unit) {
		total_distance = (total_distance * 62) / 100;
	}
	return total_distance;
}

// Function handles minute triggering
static void minute_tick_handler(struct tm *tick_time, TimeUnits units_changed) {
	char current_time[6];

	if (!tick_time) {
		time_t now = time(NULL);
		tick_time = localtime(&now);
		update_layer_content(DATE_L, (struct LayerData) { { .tick_time = tick_time }, "%a %e" });
		// Reset if dates dont match
		strftime(current_date, sizeof(current_date), "%a %e", tick_time);
		if (strcmp(last_date, current_date) != 0) {
			total_steps = 0;
			steps_per_minute = 0;
			active_minutes = 0;
			step_engaged = false;
			update_layer_content(STEPS_L, (struct LayerData) { { .number = total_steps }, NULL });
			update_layer_content(ACTIVE_L, (struct LayerData) { { .number = active_minutes }, NULL });
		}
	} else {
		if (is_tracking) {
			if (sleep_mode) {
				if (steps_per_minute == 0) {
					deep_sleep_minutes++;
				} else if (steps_per_minute > 0 && steps_per_minute <= 20) {
					light_sleep_minutes++;
				}
				update_active_sleep();
			} else {
				if (steps_per_minute >= 60) {
					active_minutes++;
					update_layer_content(ACTIVE_L, (struct LayerData) { { .number = active_minutes }, NULL });
				}
			}
		} else {
			// Enable tracking on every new minute
			is_tracking = true;
		}
		steps_per_minute = 0;
	}

	// Display time according on time display preferences
	if (clock_is_24h_style()) {
		update_layer_content(TIME_L, (struct LayerData) { { .tick_time = tick_time }, "%R" });
	} else {
		strftime(current_time, sizeof(current_time), "%I:%M", tick_time);
		if (current_time[0] == '0') memmove(current_time, &current_time[1], sizeof(current_time) - 1);
		update_layer_content(TIME_L, (struct LayerData) { { .string = current_time }, NULL });
	}

	// Reset on new day
	strftime(current_time, sizeof(current_time), "%R", tick_time);
	if (strcmp(current_time, "00:00") == 0) {
		total_steps = 0;
		steps_per_minute = 0;
		active_minutes = 0;
		step_engaged = false;
		update_layer_content(DATE_L, (struct LayerData) { { .tick_time = tick_time }, "%a %e" });
		update_layer_content(STEPS_L, (struct LayerData) { { .number = total_steps }, NULL });
		update_layer_content(ACTIVE_L, (struct LayerData) { { .number = active_minutes }, NULL });
		// Update the date so it can be written to persisting storage on exit
		strftime(current_date, sizeof(current_date), "%a %e", tick_time);
	}

	update_layer_content(DISTANCE_L, (struct LayerData) { { .number = get_total_distance() }, NULL });
}

static void update_tracking_status(bool sleep) {
	if (sleep) {
		update_layer_content(TRACKING_L, (struct LayerData) { { .string = "SLP" }, NULL });
	} else {
		update_layer_content(TRACKING_L, (struct LayerData) { { .string = "ACT" }, NULL });
	}
}

// Function handles battery status changes
static void battery_data_handler(BatteryChargeState charge) {
	update_layer_content(BATTERY_L, (struct LayerData) { { .number = charge.charge_percent }, NULL });
}

static void update_active_sleep() {
	if (sleep_view) {
		update_layer_content(ACTIVE_L, (struct LayerData) { { .number = deep_sleep_minutes }, NULL });
		update_layer_content(SLEEP_L, (struct LayerData) { { .number = light_sleep_minutes }, NULL });
	} else {
		update_layer_content(ACTIVE_L, (struct LayerData) { { .number = active_minutes }, NULL });
		update_layer_content(SLEEP_L, (struct LayerData) { { .number = (deep_sleep_minutes + light_sleep_minutes) }, NULL });
	}
}

static void reset_active_sleep() {
	sleep_view = false;
	update_active_sleep();
}

static void sleep_view_handler(AccelAxisType axis, int32_t direction) {
	if (axis == ACCEL_AXIS_Y && direction == 1 && sleep_view == false) {
		sleep_view = true;
		update_active_sleep();
		app_timer_register(5000, reset_active_sleep, NULL);
	}
}

static void tuple_changed_callback(const uint32_t key, const Tuple* tuple_new, const Tuple* tuple_old, void* context) {
	int value = tuple_new->value->uint8;
	switch (key) {
		case setting_distance:
			distance_unit = value;
			update_layer_content(DISTANCE_L, (struct LayerData) { { .number = get_total_distance() }, NULL });
			break;
		default:
			break;
	}
}

static void app_error_callback(DictionaryResult dict_error, AppMessageResult app_message_error, void* context) {
	APP_LOG(APP_LOG_LEVEL_DEBUG, "app error %d", app_message_error);
}

static void init(void) {
	// First time variables
	total_steps = 0;
	steps_per_minute = 0;
	active_minutes = 0;
	deep_sleep_minutes = 0;
	light_sleep_minutes = 0;
	step_engaged = false;
	sleep_mode = false;
	is_tracking = false;
	sleep_view = false;
	distance_unit = false;

	// Read from persisting storage
	if (persist_exists(TOTAL_STEPS_KEY)) total_steps = persist_read_int(TOTAL_STEPS_KEY);
	if (persist_exists(ACTIVE_MINUTES_KEY)) active_minutes = persist_read_int(ACTIVE_MINUTES_KEY);
	if (persist_exists(DEEP_SLEEP_MINUTES_KEY)) deep_sleep_minutes = persist_read_int(DEEP_SLEEP_MINUTES_KEY);
	if (persist_exists(LIGHT_SLEEP_MINUTES_KEY)) light_sleep_minutes = persist_read_int(LIGHT_SLEEP_MINUTES_KEY);
	if (persist_exists(SLEEP_MODE_KEY)) sleep_mode = persist_read_bool(SLEEP_MODE_KEY);
	if (persist_exists(LAST_DATE_KEY)) persist_read_string(LAST_DATE_KEY, last_date, sizeof(last_date));
	if (persist_exists(DISTANCE_UNIT_KEY)) distance_unit = persist_read_bool(DISTANCE_UNIT_KEY);

	// Subscribe to various events
	accel_service_set_sampling_rate(ACCEL_SAMPLING_25HZ);
	accel_data_service_subscribe(SAMPLE_COUNT, accel_data_handler);
	battery_state_service_subscribe(battery_data_handler);
	tick_timer_service_subscribe(MINUTE_UNIT, minute_tick_handler);
	accel_tap_service_subscribe(sleep_view_handler);

	init_gui();

	// app communication
	Tuplet tuples[] = {
		TupletInteger(setting_distance, distance_unit)
	};
	app_message_open(160, 160);
	app_sync_init(&app, buffer, sizeof(buffer), tuples, ARRAY_LENGTH(tuples), tuple_changed_callback, app_error_callback, NULL);

	battery_data_handler(battery_state_service_peek());
	update_tracking_status(sleep_mode);
	update_layer_content(STEPS_L, (struct LayerData) { { .number = total_steps }, NULL });
	update_layer_content(ACTIVE_L, (struct LayerData) { { .number = active_minutes }, NULL });
	update_layer_content(SLEEP_L, (struct LayerData) { { .number = (deep_sleep_minutes + light_sleep_minutes) }, NULL });

	// Show the time immediately after start of watchface
	minute_tick_handler(NULL, MINUTE_UNIT);
}

static void deinit(void) {
	// Write to persisting storage
	persist_write_int(TOTAL_STEPS_KEY, total_steps);
	persist_write_int(ACTIVE_MINUTES_KEY, active_minutes);
	persist_write_int(DEEP_SLEEP_MINUTES_KEY, deep_sleep_minutes);
	persist_write_int(LIGHT_SLEEP_MINUTES_KEY, light_sleep_minutes);
	persist_write_bool(SLEEP_MODE_KEY, sleep_mode);
	persist_write_string(LAST_DATE_KEY, current_date);
	persist_write_bool(DISTANCE_UNIT_KEY, distance_unit);

	app_sync_deinit(&app);

	// Unsubscribe from subscribed events
	accel_data_service_unsubscribe();
	battery_state_service_unsubscribe();
	tick_timer_service_unsubscribe();
	accel_tap_service_unsubscribe();

	deinit_gui();
}

int main(void) {
	init();

	app_event_loop();

	deinit();
}